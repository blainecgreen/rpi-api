from threading import Thread
from time import sleep

from bottle import get, post, request, run
from gpiozero import Buzzer, LED
import RPi.GPIO as GPIO


# ordered list of gpio pins for sequential leds
led_order = [27, 24, 17, 18, 23]

buzzer_pin = 22
servo_pin = 4

# initialize leds
leds = {}
for pin in led_order:
    leds[pin] = LED(pin)

# initialize buzzer
buzzer = Buzzer(buzzer_pin)

# initialize the servo
GPIO.setmode(GPIO.BCM)
GPIO.setup(servo_pin, GPIO.OUT)


@post('/')
def hello():
    print("Hello. Your http server is running.")


@post('/led/on')
def turn_on():
    led = leds.get(request.json.get('pin_id'))
    led.on()


@post('/led/off')
def turn_off():
    led = leds.get(request.json.get('pin_id'))
    led.off()


@post('/led/blink')
def do_blink():
    led = leds.get(request.json.get('pin_id'))
    task = Thread(target=blink, args=(led,), daemon=True)
    task.start()


@post('/led/sequence')
def do_sequence_blink():
    task = Thread(target=sequence_blink, daemon=True)
    task.start()


@post('/servo/open')
def do_open():
    do_servo_max()


def do_servo_max():
    # initialize pulse width modulation
    servo = GPIO.PWM(servo_pin, 50)
    servo.start(12.0)
    sleep(0.5)
    servo.stop()


@post('/servo/close')
def do_close():
    do_servo_mid()


def do_servo_mid():
    # initialize pulse width modulation
    servo = GPIO.PWM(servo_pin, 50)
    servo.start(7.5)
    sleep(0.5)
    servo.stop()


@post('/led/row')
def light_row():
    task = Thread(target=row_light, daemon=True)
    task.start()


@post('/led/row/sequence')
def do_row_sequence():
    task = Thread(target=row_sequence, daemon=True)
    task.start()


@post('/servo/test')
def do_spin():
    do_servo_mid()
    do_servo_max()
    do_servo_mid()


def row_sequence():
    row_light()
    sequence_blink()


def row_light():
    # turn everything off
    led_off_all()

    for x in range(0, 3):
        # turn on one by one in reverse order
        for index in reversed(led_order):
            buzzer.on()
            leds[index].on()
            sleep(0.25)
            buzzer.off()
            sleep(0.25)

        # blink all
        for x in range(0, 2):
            led_off_all()
            sleep(0.25)
            led_on_all()
            buzzer.on()
            sleep(0.25)
            buzzer.off()

        # turn off one by one in order
        for index in led_order:
            leds[index].off()
            sleep(0.5)


def sequence_blink():
    # turn everything off
    led_off_all()

    for x in range(0, 5):
        # cycle through leds in reverse order
        sleep(0.1)
        for index in reversed(led_order):
            buzzer.on()
            leds[index].on()
            sleep(0.05)
            buzzer.off()
            sleep(0.05)
            leds[index].off()

    # blink all
    for x in range(0, 3):
        sleep(0.25)
        buzzer.on()
        led_on_all()
        sleep(0.25)
        buzzer.off()
        led_off_all()


def led_off_all():
    for led in list(leds.values()):
        led.off()


def led_on_all():
    for led in list(leds.values()):
        led.on()


def blink(led):
    led.off()
    sleep(0.25)
    led.on()
    buzzer.on()
    sleep(1)
    led.off()
    buzzer.off()


run(host='localhost', port=8080, debug=True)
