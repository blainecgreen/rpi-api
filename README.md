# rpi-api

## Technology
* Python3
* Bottle
* RPi.GPIO

## Setup 
* Create a virtual environment
* Activate environment
* Install requirements

Set up Pi LED circuit

LED on pin 18:
```
curl -i -d '{"pin_id":18}' -H "Content-Type: application/json" -X POST localhost:8080/led/off
``` 

# Headless Raspberry Pi setup
Load the Raspian Linux distribution on a flash card: 
https://www.raspberrypi.org/documentation/installation/installing-images/README.md


Enable ssh and configure wifi on the SD card directly:
https://desertbot.io/blog/headless-raspberry-pi-3-bplus-ssh-wifi-setup
```
country=US
update_config=1
ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev

network={
    ssid="[[network]]"
    psk="[[password]]"
}
```

Power on the Raspberry pi and connect over ssh.

From the ssh connection, configure the raspberry pi to allow VNC connections:
https://howtoraspberrypi.com/raspberry-pi-vnc/

Install a VNC viewer on the remote machine.

Connect to the Raspberry pi from VNC on the remote machine.

## Tools
Raspian OS: https://www.raspberrypi.org/downloads/raspbian/  
Balena SD card flasher: https://www.balena.io/etcher/  
RealVNC viewer: https://www.realvnc.com/en/connect/download/viewer/macos/  

## Update and install dependencies
```
sudo apt-get update -y
sudo apt-get upgrade -y
sudo apt-get install python3
sudo apt-get install git
```